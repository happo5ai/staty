namespace MotorSample.States
{
    using System;

    using MotorSample.Configuration;
    using Staty;

    public class Idle : State<MotorState, MotorEvents, MotorData>
    {
        public Idle(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
            : base(MotorState.Idle, motorStateMachine)
        {
        }

        public override void Enter(MotorEvents transitionEvent, MotorData eventData)
        {
            Console.WriteLine("Entering Idle state with event " + transitionEvent);
        }

        public override void Exit(MotorEvents transitionEvent)
        {
            Console.WriteLine("Exiting Idle state");
        }
    }
}