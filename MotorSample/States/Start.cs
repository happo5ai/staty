﻿namespace MotorSample.States
{
    using System;

    using MotorSample.Configuration;
    using Staty;

    public class Start : State<MotorState, MotorEvents, MotorData>
    {
        public Start(IStateTransitioner<MotorEvents, MotorData> motorStateMachine)
            : base(MotorState.Start, motorStateMachine)
        {
        }

        public override void Enter(MotorEvents transitionEvent, MotorData eventData)
        {
            Console.WriteLine("Entering Start state with event " + transitionEvent + " and setting speed to " + eventData.MotorSpeed);
        }

        public override void Exit(MotorEvents transitionEvent)
        {
            Console.WriteLine("Exiting Start state");
        }
    }
}