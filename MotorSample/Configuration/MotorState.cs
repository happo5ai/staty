namespace MotorSample.Configuration
{
    /// <summary>
    /// Enumeration of all states, that a motor can currently be in
    /// </summary>
    public enum MotorState
    {
        Idle,
        Stop,
        Start,
        ChangeSpeed
    }
}