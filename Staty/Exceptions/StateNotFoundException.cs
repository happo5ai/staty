namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception that is thrown, if a state was not found in the state-provider
    /// </summary>
    public class StateNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StateNotFoundException"/> class.
        /// </summary>
        /// <param name="stateName">The human-readable name of a state</param>
        public StateNotFoundException(string stateName)
            : base("The state " + stateName + " could not be found in the StateMachineConfiguration")
        {
        }
    }
}