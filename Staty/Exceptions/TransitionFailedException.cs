namespace Staty.Exceptions
{
    using System;
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Exception, that will be thrown, if the state machine failed to perform a transition (e.g. due to unhandled exception in a states enter method)
    /// </summary>
    public class TransitionFailedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransitionFailedException"/> class.
        /// </summary>
        /// <param name="message">An message to explain the exception</param>
        /// <param name="innerException">The inner exception that was originally caught</param>
        public TransitionFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}