﻿namespace Staty.Exceptions
{
    using System;

    /// <summary>
    /// Exception that is thrown, if a state-enumeration was used in two states
    /// </summary>
    public class StateEnumMustBeUniqueException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StateEnumMustBeUniqueException"/> class.
        /// </summary>
        public StateEnumMustBeUniqueException()
            : base("Each state must have a unique value for the StateEnum property, but two or more states were given that share the same value. Check your states for copy-paste errors")
        {
        }
    }
}