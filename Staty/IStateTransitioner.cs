﻿namespace Staty
{
    using Staty.Exceptions;

    /// <summary>
    /// Classes that implement this interface must process internal and external events
    /// </summary>
    public interface IStateTransitioner<in TEventEnum, in TEventData>
        where TEventEnum : struct
    {
        /// <summary>
        /// Triggers the specified external event. This event locks the entire state-machine
        /// while the transition is ongoing. This ensures that multiple events can happen
        /// concurrently and are handled in a thread-safe way.
        /// </summary>
        /// <param name="externalEvent">The external event that should be fired</param>
        /// <param name="eventData">And optional event-data object</param>
        /// <exception cref="TransitionNotFoundException">Thrown, if the state-machine is misconfigured and a requested transition was not found (which happens if the provided combination of state and event was not mapped to a transition).</exception>
        /// <exception cref="TransitionFailedException">Thrown, if the actual transition failed</exception>
        /// <exception cref="FinalStateTransitionException">Thrown, if an event occurs, after the state-machine has hit its final state</exception>
        void ExternalEvent(TEventEnum externalEvent, TEventData eventData);

        /// <summary>
        /// Triggers the specified internal event. This event does not lock the state-machine.
        /// The transition will be enqueued and processed after the current operation has completed.
        /// Which destination state should be transitioned to, is determined by the StateMachineConfiguration
        /// </summary>
        /// <param name="internalEvent">The internal event that should be fired</param>
        /// <param name="eventData">And optional event-data object</param>
        void InternalEvent(TEventEnum internalEvent, TEventData eventData);
    }
}