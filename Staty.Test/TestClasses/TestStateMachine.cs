﻿namespace StatyTest.TestClasses
{
    using Staty;
    using Staty.Configuration;

    class TestStateMachine : StateMachine<TestState, TestStateEnum, TestEventEnum, TestEventData>
    {
        public TestStateMachine(
            IStateMachineConfiguration<TestStateEnum, TestEventEnum, TestEventData> stateMachineConfiguration,
            IStateProvider<TestState, TestStateEnum, TestEventEnum, TestEventData> stateProvider, 
            TestStateEnum initialState, 
            TestStateEnum? finalState = null)
            : base(stateMachineConfiguration, stateProvider, initialState, finalState)
        {
        }
    }
}
