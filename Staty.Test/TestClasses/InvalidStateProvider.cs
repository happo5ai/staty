namespace StatyTest.TestClasses
{
    using Staty;

    public class InvalidStateProvider : StateProvider<TestState, TestStateEnum, TestEventEnum, TestEventData>
    {
        protected override TestState[] GetStates(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
        {
            return new TestState[] { new State1(null) };
        }
    }
}