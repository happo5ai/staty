namespace StatyTest.TestClasses
{
    using System;

    using Staty;

    public class State5ThatThrowsExceptionOnEnter : TestState
    {
        public State5ThatThrowsExceptionOnEnter(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner)
            : base(stateTransitioner, TestStateEnum.State5)
        {
        }

        public override void Enter(TestEventEnum transitionEvent, TestEventData eventData)
        {
            throw new Exception("Testing an unhandled exception when entering this state");
        }
    }
}