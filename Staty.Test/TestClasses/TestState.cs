namespace StatyTest.TestClasses
{
    using Staty;

    public class TestState : State<TestStateEnum, TestEventEnum, TestEventData>
    {
        protected TestState(IStateTransitioner<TestEventEnum, TestEventData> stateTransitioner, TestStateEnum stateEnum)
            : base(stateEnum, stateTransitioner)
        {
        }
    }
}