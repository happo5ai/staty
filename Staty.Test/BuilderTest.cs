﻿namespace StatyTest
{
    using NUnit.Framework;
    using Staty;
    using Staty.Configuration;
    using Staty.Exceptions;
    using StatyTest.TestClasses;

    [TestFixture]
    public class BuilderTest
    {
        [Test]
        public void CreateConfiguration_MapEvent_ExpectCorrectResults()
        {
            // Act
            var configuration = new StateMachineConfigurationBuilder<TestStateEnum, TestEventEnum, TestEventData>()
                 .When(TestEventEnum.Event1).HappensIn(TestStateEnum.State1).GoTo(TestStateEnum.State2)
                 .When(TestEventEnum.Event1).HappensIn(TestStateEnum.State2).GoTo(TestStateEnum.State3)
                 .When(TestEventEnum.Event1).HappensIn(TestStateEnum.State3).IgnoreEvent()
                 .When(TestEventEnum.Event2).HappensIn(TestStateEnum.State2).ThrowInvalidTransitionException()
                 .When(TestEventEnum.InternalEvent1).HappensIn(TestStateEnum.State1).ThrowInvalidTransitionException()
                 .When(TestEventEnum.InternalEvent1).HappensIn(TestStateEnum.State3).IgnoreEvent()
                 .Build();

            // Assert
            configuration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1).PerformTransition(PerformTransition1, TestEventEnum.Event1, null);
            configuration.MapEvent(TestStateEnum.State2, TestEventEnum.Event1).PerformTransition(PerformTransition2, TestEventEnum.Event1, null);
            Assert.That(() => configuration.MapEvent(TestStateEnum.State2, TestEventEnum.Event2).PerformTransition(null, TestEventEnum.Event2, null), Throws.InstanceOf<InvalidTransitionException>());
        }

        [Test]
        public void AlternativeConfiguration_ConfigureStateMachine_ExpectCorrectConfiguration()
        {
            // Arrange
            var configuration = new StateMachineConfigurationBuilder<TestStateEnum, TestEventEnum, TestEventData>()
                .WhenIn(TestStateEnum.State1).AndReceived(TestEventEnum.Event1).GoTo(TestStateEnum.State2)
                .WhenIn(TestStateEnum.State2).AndReceived(TestEventEnum.Event1).GoTo(TestStateEnum.State3)
                .WhenIn(TestStateEnum.State3).AndReceived(TestEventEnum.Event1).IgnoreEvent()
                .WhenIn(TestStateEnum.State2).AndReceived(TestEventEnum.Event2).ThrowInvalidTransitionException()
                .WhenIn(TestStateEnum.State1).AndReceived(TestEventEnum.InternalEvent1).ThrowInvalidTransitionException()
                .WhenIn(TestStateEnum.State3).AndReceived(TestEventEnum.InternalEvent1).IgnoreEvent()
                .Build();

            // Act

            // Assert
            configuration.MapEvent(TestStateEnum.State1, TestEventEnum.Event1).PerformTransition(PerformTransition1, TestEventEnum.Event1, null);
            configuration.MapEvent(TestStateEnum.State2, TestEventEnum.Event1).PerformTransition(PerformTransition2, TestEventEnum.Event1, null);
            Assert.That(() => configuration.MapEvent(TestStateEnum.State2, TestEventEnum.Event2).PerformTransition(null, TestEventEnum.Event2, null), Throws.InstanceOf<InvalidTransitionException>());
        }

        private void PerformTransition1(TestStateEnum targetState, TestEventEnum occurredEvent, TestEventData eventData)
        {
            Assert.That(targetState, Is.EqualTo(TestStateEnum.State2));
            Assert.That(occurredEvent, Is.EqualTo(TestEventEnum.Event1));
            Assert.That(eventData, Is.Null);
        }

        private void PerformTransition2(TestStateEnum targetState, TestEventEnum occurredEvent, TestEventData eventData)
        {
            Assert.That(targetState, Is.EqualTo(TestStateEnum.State3));
            Assert.That(occurredEvent, Is.EqualTo(TestEventEnum.Event1));
            Assert.That(eventData, Is.Null);
        }

        [Test]
        public void InvalidConfigurationTest()
        {
            // Act & Assert
            Assert.That(() => new StateMachineConfigurationBuilder<TestStateEnum, TestEventEnum, TestEventData>()
                 .When(TestEventEnum.Event2).HappensIn(TestStateEnum.State2).ThrowInvalidTransitionException()
                 .When(TestEventEnum.Event2).HappensIn(TestStateEnum.State2).ThrowInvalidTransitionException(), Throws.InstanceOf<DuplicationConfigurationException>());
        }
    }
}